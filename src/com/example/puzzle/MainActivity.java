package com.example.puzzle;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

    Button btnRate;
    Button btnPlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Game) this.getApplication()).refreshImages();

        SharedPreferences settings = getSharedPreferences("UserInfo", 0);
        int LastLevelNum = Integer.valueOf(settings.getString("LastLevelNum", "1"));


        ((Game) this.getApplication()).setInitLastLevelNum(LastLevelNum);
        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(this);

    }

     public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnPlay:
                Intent intent = new Intent(this, Choice.class);
                startActivity(intent);
            default:
            break;
        }
    }

}

