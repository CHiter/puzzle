package com.example.puzzle;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class Game extends Application {

    //Префикс картинок
    public static final String IMAGE_FILE_PREFIX = "puzzle_";

    //Номер уровня
    private static int levelNum;

    //Пройденый уровень
    private static int lastLevelNum;

    //Массив уровней
    private static List<Level> levels = new ArrayList<Level>();

    public void refreshImages() {

        Field[] drawables = R.drawable.class.getFields();
        for (Field f : drawables) {
            try {
                if (f.getName().startsWith(IMAGE_FILE_PREFIX)){
                    Drawable img = getResources().getDrawable(f.getInt(getResources().getIdentifier(f.getName(), "drawable", this.getPackageName())));
                    levels.add(new Level(((BitmapDrawable)img).getBitmap()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getLastLevelNum(){
        return lastLevelNum;
    }

    public void setLastLevelNum(){
        int level = getLevelNum()+1;
        if(lastLevelNum == level) {
            lastLevelNum = level+1;
            SharedPreferences settings = getSharedPreferences("UserInfo", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("LastLevelNum", String.valueOf(lastLevelNum));
            editor.commit();
        }
    }

    public void setInitLastLevelNum(int level){
        lastLevelNum = level;
    }

    public int getLevelNum(){
        return levelNum;
    }

    public void setLevelNum(int imagePosition){
        levelNum = imagePosition;
    }

    public Level getLevel(){
        return levels.get(levelNum);
    }

    public Level getLevelByNum(int imagePosition){
        return levels.get(imagePosition);
    }

    public List<Level> getLevels() {
        return levels;
    }

    public int getLevelCount(){
        return levels.size();
    }

}