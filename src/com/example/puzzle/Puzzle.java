package com.example.puzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;


public class Puzzle extends Activity {

    private GridView mGrid;
    private GridAdapter mAdapter;

    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.puzzle);

        mGrid = (GridView)findViewById(R.id.field);
        mGrid.setNumColumns(3);
        mGrid.setEnabled(true);

        mGrid.setBackgroundColor(Color.WHITE);
        mGrid.setVerticalSpacing(1);
        mGrid.setHorizontalSpacing(1);

        WindowManager wm = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Bitmap puzzle = ((Game) getApplication()).getLevel().puzzle;

        mAdapter = new GridAdapter(this, puzzle, 3, 3, (width/3) -2);

        mGrid.setAdapter(mAdapter);

        mGrid.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    GridView parent = (GridView) v;

                    int x = (int) event.getX();
                    int y = (int) event.getY();

                    final int position = parent.pointToPosition(x, y);
                    if (position > AdapterView.INVALID_POSITION) {

                        int count = parent.getChildCount();
                        for (int i = 0; i < count; i++) {
                            View curr = parent.getChildAt(i);
                            curr.setOnDragListener(new View.OnDragListener() {

                                @Override
                                public boolean onDrag(View v, DragEvent event) {

                                    boolean result = true;
                                    int action = event.getAction();
                                    switch (action) {
                                        case DragEvent.ACTION_DRAG_STARTED:
                                            break;
                                        case DragEvent.ACTION_DRAG_LOCATION:
                                            break;
                                        case DragEvent.ACTION_DRAG_ENTERED:
                                            v.setAlpha(0.5f);
                                            break;
                                        case DragEvent.ACTION_DRAG_EXITED:
                                            v.setAlpha(1f);
                                            break;
                                        case DragEvent.ACTION_DROP:
                                            if (event.getLocalState() == v) {
                                                result = false;
                                            } else {
                                                View droped = (View) event.getLocalState();
                                                Bitmap dropItem = ((DragGridItemHolder) droped.getTag()).item;

                                                GridView parent = (GridView) droped.getParent();
                                                GridAdapter adapter = (GridAdapter) parent.getAdapter();
                                                ArrayList<Bitmap> items = adapter.getItems();

                                                int dropIndex = items.indexOf(dropItem);

                                                View target = v;
                                                Bitmap targetItem = ((DragGridItemHolder) target.getTag()).item;
                                                int targetIndex = items.indexOf(targetItem);

                                                items.set(dropIndex, targetItem);
                                                items.set(targetIndex, dropItem);

                                                adapter.notifyDataSetChanged();
                                                v.setAlpha(1f);

                                                ArrayList<String> answers = adapter.getAnswers();
                                                ArrayList<String> currenet_answers = adapter.getCurrenet_answers();
                                                int pos = 0;
                                                int count = 0;
                                                for (String answer : answers) {
                                                    if (answer.equals(currenet_answers.get(pos))) {
                                                        count++;
                                                    }
                                                    pos++;
                                                }
                                                if (count == 9) {
                                                    ((Game) getApplication()).setLastLevelNum();
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(Puzzle.this);
                                                    builder.setTitle("Information").setMessage("Congratulations!").setCancelable(true);
                                                    AlertDialog dialog = builder.create();
                                                    dialog.show();
                                                    Intent intent = new Intent(Puzzle.this, Choice.class);
                                                    startActivity(intent);
                                                }
                                            }
                                            break;
                                        case DragEvent.ACTION_DRAG_ENDED:

                                        default:
                                            result = false;
                                            v.setAlpha(1f);
                                            break;
                                    }
                                    return result;
                                }
                            });
                        }

                        int relativePosition = position - parent.getFirstVisiblePosition();


                        View target = parent.getChildAt(relativePosition);

                        DragGridItemHolder holder = (DragGridItemHolder) target.getTag();
                        Bitmap currentItem = holder.item;
                        ClipData data = ClipData.newPlainText("DragData", "dadsada");
                        target.startDrag(data, new View.DragShadowBuilder(target), target, 0);


                    }
                }
                return false;
            }
        });
    }
}
