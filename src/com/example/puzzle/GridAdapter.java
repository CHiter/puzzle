package com.example.puzzle;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;


class GridAdapter extends BaseAdapter {

    private Context mContext;
    int rows, columns, tileSize;
    Bitmap original, scaledImage;
    ArrayList<Bitmap> unservedSlices;
    ArrayList<String> answers;
    ArrayList<String> currenet_answers;

    public GridAdapter(Context c, Bitmap original, int rows, int columns, int tileSize){
        mContext = c;
        this.original = original;
        this.columns = columns;
        this.rows = rows;
        this.tileSize = tileSize;
        unservedSlices = new ArrayList<Bitmap>();
        answers = new ArrayList<String>();
        sliceOriginal();
    }

    protected void sliceOriginal() {
        int fullWidth = tileSize * rows;
        int fullHeight = tileSize * columns;
        scaledImage = Bitmap.createScaledBitmap(original, fullWidth, fullHeight, true);
        int x, y;
        int pos = 0;
        Bitmap bitmap;
        for (int rowI=0; rowI<rows; rowI++) {
            for (int colI=0; colI<columns; colI++) {
                y = rowI * tileSize;
                x = colI * tileSize;
                bitmap = Bitmap.createBitmap(scaledImage, x, y, tileSize, tileSize);
                answers.add(pos, String.valueOf(bitmap));
                unservedSlices.add(pos, bitmap);
                pos++;
            }
        }
        Collections.shuffle(unservedSlices);
    }

    public ArrayList<Bitmap> getItems() {
        return unservedSlices;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public ArrayList<String> getCurrenet_answers() {
        currenet_answers = new ArrayList<String>();
        int pos=0;
        for(Bitmap image :unservedSlices){
            currenet_answers.add(pos, String.valueOf(image));
            pos++;
        }
        return currenet_answers;
    }


    @Override
    public int getCount() {
        return unservedSlices.size();
    }

    @Override
    public Object getItem(int position) {
        return unservedSlices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DragGridItemHolder holder;
        if (convertView == null) {
            holder = new DragGridItemHolder();

            ImageView img = new ImageView(mContext);
            holder.image = img;
            convertView = img;
            convertView.setTag(holder);

        } else {
            holder = (DragGridItemHolder) convertView.getTag();
        }
        holder.item = unservedSlices.get(position);
        holder.image.setImageBitmap(unservedSlices.get(position));
        return convertView;

    }


}
