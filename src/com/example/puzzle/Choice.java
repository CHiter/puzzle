package com.example.puzzle;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.content.Intent;
import java.lang.String;


public class Choice extends Activity {
     
    Gallery mGallery;
    TextView mTextView;
    ImageAdapter mImageAdapter;

    int currentLevelNum;

    @Override
    public void onResume() {
        super.onResume();
        mImageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.choice);

        currentLevelNum = ((Game) getApplication()).getLastLevelNum();

        // Текстовое поле, в которое выводится номер выделенного элемента
        mTextView = (TextView) findViewById(R.id.mTextView);
        mGallery = (Gallery) findViewById(R.id.gallery);

        Typeface type = Typeface.createFromAsset(getAssets(), "Axis_rus.ttf");
        mTextView.setTypeface(type);

        // Устанавливаем адаптер
        mImageAdapter = new ImageAdapter(this);  
        mGallery.setAdapter(mImageAdapter);

        // Выделяем элемент по середине
        mGallery.setSelection(currentLevelNum-1);

        // Устанавливаем действия, которые будут выполнены при клике на элемент
        mGallery.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                if (view.getTag() == "active") {
                    ((Game) getApplication()).setLevelNum(position);
                    Intent intent = new Intent(view.getContext(), Puzzle.class);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Choice.this);
                    builder.setTitle("Information").setMessage("Please complete last level before open new").setCancelable(true);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }

        });

        // Устанавливаем действия, которые будут выполнены при выделении элемента
        mGallery.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                mTextView.setText(String.valueOf(position + 1) + " / " + String.valueOf(parent.getCount()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                mTextView.setText("");
            }

        });
    }
     
    // Класс адаптера
    public class ImageAdapter extends BaseAdapter {

        int mGalleryItemBackground;
        private Context mContext;
        private Resources mRes;

        public ImageAdapter(Context c) {
            mContext = c;
            mRes = mContext.getResources();
            TypedArray attr = mContext.obtainStyledAttributes(R.styleable.MyGallery);
            mGalleryItemBackground = attr.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 0);
            attr.recycle();
        }
 
        public int getCount() {
            return ((Game) getApplication()).getLevelCount();
        }
 
        public Object getItem(int position) {
            return position;
        }
 
        public long getItemId(int position) {
            return position;
        }
 
        public View getView(int position, View convertView, ViewGroup parent) {

            ImageView imageView = new ImageView(mContext);
            imageView.setImageBitmap(((Game) getApplication()).getLevelByNum(position).puzzle);
            if(position < currentLevelNum) {
                imageView.setTag("active");
            }else{
                imageView.setAlpha(0.5f);
            }

            // Позиционирование по центру
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setPadding(20, 20, 20, 20);

            // Размер по содержимому
            imageView.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            return imageView;

        }
    }
}